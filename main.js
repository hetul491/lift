const minFloor = -2;
const maxFloor = 15;
var currentFloor = 0;
var upTripFloors = [];
var downTripFloors = [];

function setTrips(start, end) {
	if (start === end) return;

	if (start > maxFloor || start < minFloor) {
		console.error("Invalid Start Floor");
		return;
	}

	if (end > maxFloor || end < minFloor) {
		console.error("Invalid End Floor");
		return;
	}

	start < end
		? upTripFloors.push(start, end)
		: downTripFloors.push(start, end);

	upTripFloors.sort((a, b) => a - b);
	downTripFloors.sort((a, b) => a - b);
}

var startInterval = () => setInterval(() => startTransaction(), 1000);

function startTransaction() {
	if (upTripFloors.length === 0 && downTripFloors.length === 0) return;

	if (upTripFloors.some((lift) => currentFloor <= lift)) {
		if (upTripFloors.some((lift) => lift < 0)) {
			currentFloor--;
			console.log("Current Floor: ", currentFloor);

			if (upTripFloors.some((lift) => currentFloor === lift)) {
				console.log("Door Open");
				upTripFloors = upTripFloors.filter(
					(item) => item !== currentFloor
				);
			}
		} else {
			if (upTripFloors.length >= 0)
				console.log("Current floor: ", currentFloor);

			if (upTripFloors.some((lift) => currentFloor === lift)) {
				console.log("Door Open");
				upTripFloors = upTripFloors.filter(
					(item) => item !== currentFloor
				);
			}

			if (upTripFloors.length > 0) currentFloor++;
		}
	} else {
		if (upTripFloors.some((lift) => lift > 0)) {
			currentFloor--;
			console.log("Current Floor: ", currentFloor);

			if (upTripFloors.some((lift) => currentFloor === lift)) {
				console.log("Door Open");
				upTripFloors = upTripFloors.filter(
					(item) => item !== currentFloor
				);
			}
		} else {
			if (downTripFloors.length > 0) currentFloor--;

			if (downTripFloors.length >= 0)
				console.log("Current floor: ", currentFloor);

			if (downTripFloors.some((lift) => currentFloor === lift)) {
				console.log("Door Open");
				downTripFloors = downTripFloors.filter(
					(item) => item !== currentFloor
				);
			}
		}
	}
}

startInterval();
setTrips(0, 10);
setTrips(6, 2);
setTrips(2, 8);
setTrips(2, 5);
setTrips(-2, 7);
setTrips(8, -2);
